<?php

/**
 * @file
 * Exposes config server administration.
 */

/**
 * config_server_admin_settings form.
 */
function config_server_admin_settings() {
  $options = drupal_map_assoc(config_get_storage_names_with_prefix());
  $form['available_config'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Configs'),
    '#default_value' => config('configtools.server')->get('available_config'),
    '#options' => $options,
    '#description' => t('Select configuration available thorugh configuration server.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Submit config_server_admin_settings form.
 */
function config_server_admin_settings_submit($form, &$form_state) {
  config('configtools.server')
    ->set('available_config', array_values(array_filter($form_state['values']['available_config'])))
    ->save();
}
