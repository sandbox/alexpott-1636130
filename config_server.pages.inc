<?php

/**
 * @file
 * Exposes global functionality for creating image styles.
 */

use Drupal\Core\Config\FileStorage;

/**
 * Serve config as Yaml.
 *
 * @param $config
 *   Config object..
 */
function config_server_view($config) {
  // We are returning Yaml, so tell the browser.
  drupal_add_http_header('Content-Type', 'application/json');
  print FileStorage::encode($config->get());
  exit;
}
