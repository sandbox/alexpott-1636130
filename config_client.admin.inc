<?php

/**
 * @file
 * Exposes config client administration.
 */

use Drupal\Core\Config\FileStorage;

/**
 * config_client_admin_settings form.
 */
function config_client_admin_settings() {
  $form['config_server_uri'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of config server'),
    '#default_value' => config('configtools.client')->get('server_uri'),
    '#description' => t('URL of configuration server.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate config_client_admin_settings form.
 */
function config_client_admin_settings_validate($form, &$form_state) {
  $server_config = _config_client_get_config_from_server(
    $form_state['values']['config_server_uri'],
    'configtools.server'
  );
  if (!$server_config) {
    form_set_error('config_server_uri', t('Unable to get list of available config from server.'));
  }
}

/**
 * Submit config_client_admin_settings form.
 */
function config_client_admin_settings_submit($form, &$form_state) {
  config('configtools.client')
    ->set('server_uri', $form_state['values']['config_server_uri'])
    ->save();
  drupal_set_message(t('Configuration server saved and availablity checked.'));
}

/**
 * config_client_import form.
 *
 * Lists config available to import.
 */
function config_client_import() {
  $server_config = _config_client_get_config_from_server(
    config('configtools.client')->get('server_uri'),
    'configtools.server'
  );

  if (is_array($server_config) && is_array($server_config['available_config'])) {
    $options = drupal_map_assoc($server_config['available_config']);
    $form['available_config'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Configs'),
      '#options' => $options,
      '#description' => t('Select configuration available thorugh configuration server.'),
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Import'),
    );
  }

  if (count($form) == 0 ){
    drupal_set_message('Incorrect server config');
    drupal_goto('admin/config/services/config_client/server');
  }
  return $form;
}

/**
 * Submit handler for config_client_import form.
 *
 * Fetches, parses and imports Yaml from config server.
 */
function config_client_import_submit($form, &$form_state) {
  
  foreach(array_filter($form_state['values']['available_config']) as $config) {
    $data = _config_client_get_config_from_server(
      config('configtools.client')->get('server_uri'),
      $config
    );
    config($config)->setData($data)->save();
    drupal_set_message(t('Imported @config.', array('@config' => $config)));
  }
}

/**
 * Fetches Yaml from the config server and parses it.
 *
 * @param $uri
 *   The uri of the config server.
 * @param $config
 *   The config to retreive.
 *
 * @return
 *   The parsed config as an array or FALSE.
 */
function _config_client_get_config_from_server($uri, $config) {
  $response = drupal_http_request($uri . '/config_server/'. $config);
  if (isset($response->code) && $response->code == 200 && isset($response->data)) {
    return FileStorage::decode($response->data);
  }
  else {
    return FALSE;
  }
}
